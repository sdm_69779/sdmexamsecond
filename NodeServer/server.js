const express = require("express");
const cors = require("cors");

const app = express();

const movieRouter = require("./router/movie");

app.use(cors('*'));
app.use(express.json());
app.use('/movie',movieRouter);

app.listen(4000,'0.0.0.0',()=>{
    console.log("server started on 4000");
})