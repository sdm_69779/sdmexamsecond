const express = require("express");
const db = require("../db");
const util = require("../utils");
const router = express.Router();

router.post("/", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;
  console.log(movie_title);
  const query =
    "insert into movie(movie_title, movie_release_date, movie_time, director_name) values (?,?,?,?)";

  db.pool.execute(
    query,
    [movie_title, movie_release_date, movie_time, director_name],
    (error, result) => {
      console.log(error);
      console.log(result);
      response.send(util.createResult(error, result));
    }
  );
});

router.get("/", (request, response) => {
  const query = "select * from movie";
  db.pool.execute(query, (errpr, data) => {
    console.log(data);
    response.send(util.createResult(errpr, data));
  });
});

router.put("/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;
  console.log(movie_id);

  const query =
    "update movie set movie_title = ?, movie_release_date = ?, movie_time = ?, director_name = ? where movie_id = ?";
  db.pool.execute(
    query,
    [movie_title, movie_release_date, movie_time, director_name, movie_id],
    (error, result) => {
      response.send(util.createResult(error, result));
    }
  );
});

router.delete("/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const query = "delete from movie where movie_id = ?";
  db.pool.execute(query, [movie_id], (error, result) => {
    response.send(util.createResult(error, result));
  });
});
module.exports = router;
